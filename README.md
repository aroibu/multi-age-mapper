# Age Mapper

This project looks at employing different neuroimaging modalities to understand patterns of brain ageging and factors influencing it.

## References
In the creation of this code, material was used from the following paper. The original code can be accessed on [GitHub](https://github.com/ha-ha-ha-han/UKBiobank_deep_pretrain/)

```
@article{peng2021accurate,
  title={Accurate brain age prediction with lightweight deep neural networks},
  author={Peng, Han and Gong, Weikang and Beckmann, Christian F and Vedaldi, Andrea and Smith, Stephen M},
  journal={Medical image analysis},
  volume={68},
  pages={101871},
  year={2021},
  publisher={Elsevier}
}
```